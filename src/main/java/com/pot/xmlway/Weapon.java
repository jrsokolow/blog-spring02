package com.pot.xmlway;

/**
 * Created by sokoloj1 on 2015-11-19.
 */
public interface Weapon {
    String getName();
    int attack();
}
