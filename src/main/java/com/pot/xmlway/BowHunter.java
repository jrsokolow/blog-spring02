package com.pot.xmlway;

/**
 * Created by sokoloj1 on 2015-11-13.
 */
public class BowHunter implements Hunter {

    int age;
    String firstName;
    Weapon bow;
    Team team;

    public BowHunter(String firstName, Weapon bow) {
        this.firstName = firstName;
        this.bow = bow;
    }

    @Override
    public void hunt() {
        System.out.println("Hunter: " + this.firstName + ", age: " + this.age + ", team: " +
                this.team.getName() + " -> is hunting via " + this.bow.getName() + " with " +
                this.bow.attack() + " attack points");
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
